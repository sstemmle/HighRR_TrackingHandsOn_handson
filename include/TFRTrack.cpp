/*!
 *  @author    Simon Stemmle
 *  @brief     Representation of reconstructed tracks
 */

#include "TFRTrack.h"

ClassImp(TFRTrack)

//----------
// Constructor with clusters
//----------
TFRTrack::TFRTrack(TFRClusters *_clusters){
  initial_state = new TFRState();
  clusters = _clusters;
  chi2 = 999999.;
  momentum = TVector3(0., 0., 0.);
  ax = 0;
  bx = 0;
  cx = 0;
  ay = 0;
  by = 0;
  cy = 0;
  fit_status = false;
  clusters = new TFRClusters();
  states_km = new TFRStates();
  fitnodes = new TFRFitnodes();
  particles = new TFRParticles();
  covariance_matrix = TMatrixD(6, 6); //up to quadratic parametrization
}

//----------
// Create fitnodes from clusters
//----------
void TFRTrack::CreateFitnodes(){

  //Clear the Fitnodes
  fitnodes = new TFRFitnodes();
  
  //Set the Fitnodes
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,                                                                                                         
  //but just this stupid way (that I even didn't used at my C course during my bachelor)                                                                                                      
  TIter it_cluster((TFRClusters*) clusters);

  TFRCluster *curr_cluster;
  
  //loop over the clusters of the current particle                                                                                                                                            
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){
    fitnodes->Add(new TFRFitnode(*curr_cluster));
  }
  
  return;
}

//----------
// Returns the state at z
//----------
TFRState* TFRTrack::GetStateAt(double z){
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)                                                                                                      
  TIter it_state((TFRStates*) states_km);

  TFRState *curr_state;
  
  //loop over the clusters of the current particle                                                       
  while((curr_state = (TFRState*) it_state.Next())){
    if(fabs(curr_state->GetZ()-z)<1e-5){
      return curr_state;
    }
  }
  
  return curr_state;
}

//----------
// Add the cluster to the vector of clusters
//----------
void TFRTrack::AddCluster(TFRCluster *cluster){

  //if the cluster is valid, add it to the clusters of the track
  if(cluster->GetZ() > -9999.)
    clusters->Add(cluster);

  return;
}

//----------
// Add the state to the vector of states used by the Kalman Filter
//----------
void TFRTrack::AddState(TFRState *state){

  //if the state is valid, add it to the states of the track
  if(state->GetZ() > -9999.)
    states_km->Add(state);

  return;
}


//----------
// Add the particle to the vector of particles
//----------
void TFRTrack::AddParticle(TFRParticle *particle){

  //if the particle is valid, add it to the associated particles
  if(particle != NULL)
    particles->Add(particle);

  return;
}
